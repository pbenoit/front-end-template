$('html').removeClass('no_js');
$('html').addClass('js');

var AppSettings = {
	DEBUGMODE: true, //change to turn on/off console.log statements
};

var Debug = {
	log: function (string, variable) {
		if (AppSettings.DEBUGMODE) {
			try { console.log(string, variable); }
			catch (e) { }
		}
	},
	warn: function (string, variable) {
		if (AppSettings.DEBUGMODE) {
			try { console.warn(string, variable); }
			catch (e) { }
		}
	},
};

var Main = {
	run: function () {
		Debug.log('Debug.log');
		Debug.warn('Debug.warn');
	}
};

$(document).ready(Main.run());