module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		banner:
		'/*! <%= pkg.title || pkg.name %> - v<%= pkg.version %> - <%= grunt.template.today("yyyy-mm-dd") %>\n' +
		' * Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author %>\n' +
		' * Licensed <%= _.pluck(pkg.licenses, "type").join(", ") %>\n */\n',
		sass: {
			dist: {
				options: {                       // Target options
					banner: '<%= banner %>'
				},
				files: {
					'css/style.css': 'scss/style.scss'
				}
			}
		},
		concat: {
			options: {
				stripBanners: true,
				banner: '<%= banner %>'
			},
			vendor: {
				src: ['scripts/vendor/*.js'],
				dest: 'scripts/vendor.min.js',
			},
			custom:{
				src: ['scripts/custom/*.js'],
				dest: 'scripts/custom.min.js',
			}
		},
		cssmin: {
			add_banner: {
				options: {
					banner: '<%= banner %>'
				},
				files: {
				'css/style.min.css': ['css/style.css']
				}
			}
		},
		uglify: {
			options: {
				banner: '<%= banner %>',
				mangle: false
			},
			dist: {
				files: {
					'scripts/custom.min.js': ['scripts/custom.min.js'],
					'css/style.min.css': ['css/style.css']
				}
			}
		},
		jshint: {
			files: ['gruntfile.js', 'src/**/*.js', 'test/**/*.js'],
			options: {
				// options here to override JSHint defaults
				globals: {
					jQuery: true,
					console: true,
					module: true,
					document: true
				}
			}
		},
		watch: {
			files: ['<%= jshint.files %>', 'scss/*.scss', 'scripts/custom/*.js', 'scripts/vendor/*.js'],
			tasks: ['jshint', 'sass', 'concat', 'uglify', 'cssmin'],
			options: {
            // Start a live reload server on the default port 35729
                livereload: true,
            },
		}
	});

	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-cssmin');

	grunt.registerTask('default', ['jshint','sass']);

	// Print a timestamp (useful for when watching)
	grunt.registerTask('timestamp', function () {
		grunt.log.subhead(Date());
	});
};